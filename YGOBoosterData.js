'use strict';
/*eslint-env node, es6*/

const order = [
  'lob' ,  'mrd',  'srl',  'psv',  'lon',  'lod',
  'pgd' ,  'mfc',  'dcr',  'ioc',  'ast',  'sod',
  'rds' ,  'fet',  'tlm',  'crv',  'een',  'soi',
  'eoj' , 'potd', 'cdip', 'ston', 'fotb', 'taev',
  'glas', 'ptdn', 'lodt', 'tdgs', 'csoc', 'crms',
  'rgbt', 'anpr', 'sovr', 'abpf', 'tshd', 'drev',
  'stbl', 'stor', 'exvc', 'genf', 'phsw', 'orcs',
  'gaov', 'redu', 'abyr', 'cblz', 'ltgy', 'numh',
  'jotl', 'shsp', 'lval', 'prio', 'duea', 'nech',
  'sece', 'cros',
];

const PACK_NO_SP_PRE_TDGS = {
  cardGroups: ['C', 'R', 'SR', 'UR', 'SCR'],
  groups   : { Cmn: ['C'], NtCmn: ['R', 'SR', 'UR', 'SCR'] },
  packDesc : ['Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'NtCmn'],
};

// const PACK_NO_SP_POST_TDGS = {
//   cardGroups: ['C', 'R', 'SR', 'UR', 'SCR',],
//   groups   : { Cmn: ['C',],  R: ['R',], NtR: ['C', 'SR', 'UR', 'SCR',], },
//   packDesc : ['Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'R', 'NtR',],
// };

const PACK_HOLO_RARE = {
  cardGroups: ['C', 'R', 'SR', 'UR', 'SCR'],
  groups   : { Cmn: ['C'],  R: ['R'], Holo: ['SR', 'UR', 'SCR'] },
  packDesc : ['Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'R', 'Holo'],
};

const PACK_SP_PRE_TDGS = {
  cardGroups: ['C', 'SP', 'R', 'SR', 'UR', 'SCR'],
  groups   : { Cmn: ['C', 'SP'], NtCmn: ['R', 'SR', 'UR', 'SCR'] },
  packDesc : ['Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'NtCmn'],
};

const PACK_SP_POST_TDGS = {
  cardGroups: ['C', 'SP', 'R', 'SR', 'UR', 'SCR'],
  groups   : { Cmn: ['C', 'SP'],  R: ['R'], NtR: ['C', 'SP', 'SR', 'UR', 'SCR'] },
  packDesc : ['Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'R', 'NtR'],
};

const PACK_HA = {
  cardGroups : ['SCR', 'SR'],
  groups: {Scr: ['SCR'], Sr: ['SR']},
  packDesc: ['Scr', 'Sr', 'Sr', 'Sr', 'Sr'],
};

const PACK_DP_6 = {
  cardGroups : ['C', 'R', 'SR', 'UR'],
  groups: {Cmn: ['C'], NtCmn: ['R', 'SR', 'UR']},
  packDesc: ['Cmn', 'Cmn', 'Cmn', 'Cmn', 'Cmn', 'NtCmn'],
};

const PACK_DP_5 = {
  cardGroups : ['C', 'R', 'SR', 'UR'],
  groups: {Cmn: ['C'], NtCmn: ['R', 'SR', 'UR']},
  packDesc: ['Cmn', 'Cmn', 'Cmn', 'Cmn', 'NtCmn'],
};

const PACK_GOLD = {
  cardGroups : ['C', 'GR'],
  groups: { C: ['C'], GR: ['GR']},
  packDesc: ['C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C',
         'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C',
         'C', 'C', 'C', 'GR', 'GR', 'GR'],
};

const PACK_LC = {
  cardGroups: ['C', 'R', 'SR', 'UR', 'SCR'],
  groups  : { C : ['C'], SR : ['SR'], UR : ['UR'], 'SCR': ['SCR'] },
  packDesc  : ['C', 'C', 'C', 'C', 'C', 'R', 'SR', 'UR', 'SCR'],
};

const PACK_MP = {
  cardGroups: ['C', 'R', 'SR', 'UR', 'SCR'],
  groups  : { C : ['C'], SR : ['SR'], UR : ['UR'], 'SCR': ['SCR'] },
  packDesc  : ['C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C',
         'C', 'C', 'R', 'SR', 'UR', 'SCR'],
};

const PACK_PREMIUM = {
  cardGroups : ['SCR', 'SR'],
  groups: {SCR: ['SCR'], SR: ['SR']},
  packDesc: ['Scr', 'SCR', 'SR', 'SR', 'SR'],
};

const PACK_STAR = {
  cardGroups: ['C'],
  groups  : { C : ['C'] },
  packDesc  : ['C', 'C', 'C'],
};


module.exports = exports = {
  order,
  packSetting: {
    'lob' : PACK_SP_PRE_TDGS,
    'mrd' : PACK_SP_PRE_TDGS,
    'srl' : PACK_SP_PRE_TDGS,
    'psv' : PACK_SP_PRE_TDGS,
    'lon' : PACK_SP_PRE_TDGS,
    'lod' : PACK_SP_PRE_TDGS,
    'pgd' : PACK_SP_PRE_TDGS,
    'mfc' : PACK_SP_PRE_TDGS,
    'dcr' : PACK_SP_PRE_TDGS,
    'ioc' : PACK_SP_PRE_TDGS,
    'ast' : PACK_SP_PRE_TDGS,
    'sod' : PACK_NO_SP_PRE_TDGS,
    'rds' : PACK_NO_SP_PRE_TDGS,
    'fet' : PACK_NO_SP_PRE_TDGS,
    'tlm' : PACK_NO_SP_PRE_TDGS,
    'crv' : PACK_NO_SP_PRE_TDGS,
    'een' : PACK_NO_SP_PRE_TDGS,
    'soi' : PACK_NO_SP_PRE_TDGS,
    'eoj' : PACK_NO_SP_PRE_TDGS,
    'potd': PACK_NO_SP_PRE_TDGS,
    'cdip': PACK_NO_SP_PRE_TDGS,
    'ston': PACK_NO_SP_PRE_TDGS,
    'fotb': PACK_NO_SP_PRE_TDGS,
    'taev': PACK_NO_SP_PRE_TDGS,
    'glas': PACK_NO_SP_PRE_TDGS,
    'ptdn': PACK_SP_PRE_TDGS,
    'lodt': PACK_SP_PRE_TDGS,
    'tdgs': PACK_SP_POST_TDGS,
    'csoc': PACK_SP_POST_TDGS,
    'crms': PACK_SP_POST_TDGS,
    'rgbt': PACK_SP_POST_TDGS,
    'anpr': PACK_SP_POST_TDGS,
    'sovr': PACK_SP_POST_TDGS,
    'abpf': PACK_SP_POST_TDGS,
    'tshd': PACK_SP_POST_TDGS,
    'drev': PACK_SP_POST_TDGS,
    'stbl': PACK_SP_POST_TDGS,
    'stor': PACK_SP_POST_TDGS,
    'exvc': PACK_SP_POST_TDGS,
    'genf': PACK_SP_POST_TDGS,
    'phsw': PACK_SP_POST_TDGS,
    'orcs': PACK_SP_POST_TDGS,
    'gaov': PACK_SP_POST_TDGS,
    'redu': PACK_SP_POST_TDGS,
    'abyr': PACK_SP_POST_TDGS,
    'cblz': PACK_SP_POST_TDGS,
    'ltgy': PACK_SP_POST_TDGS,
    'jotl': PACK_SP_POST_TDGS,
    'shsp': PACK_SP_POST_TDGS,
    'lval': PACK_SP_POST_TDGS,
    'prio': PACK_SP_POST_TDGS,
    'duea': PACK_SP_POST_TDGS,
    'nech': PACK_SP_POST_TDGS,
    'sece': PACK_SP_POST_TDGS,
    'cros': PACK_SP_POST_TDGS,
    //insert other packs here

    //other non-lineal packs
    'bp01': {
      cardGroups: ['S1', 'S2', 'S3', 'S4'],
      groups  : { S1 : ['S1'], S2 : ['S2'], S3 : ['S3'], S4 : ['S4'], all: ['S1', 'S2', 'S3', 'S4'] },
      packDesc  : ['S1', 'S2', 'S3', 'S4', 'all'],
    },
    'bp02': {
      cardGroups: ['C', 'R', 'MR'],
      groups  : { C : ['C'], R : ['R'], all : ['C', 'R', 'MR'] },
      packDesc  : ['C', 'C', 'C', 'R', 'all'],
    },
    'bpw2': {
      cardGroups: ['C', 'SR', 'UR'],
      groups  : { C : ['C'], SR : ['SR'], UR: ['UR'] },
      packDesc  : ['C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'SR', 'SR', 'SR', 'SR', 'SR', 'SR', 'UR'],
    },
    'bp03': {
      cardGroups: ['C', 'R', 'SHR'],
      groups  : { C : ['C'], R : ['R'], all : ['C', 'R', 'SHR'] },
      packDesc  : ['C', 'C', 'C', 'R', 'all'],
    },

    'WGRT': {
      cardGroups: ['C', 'SR', 'UR'],
      groups  : { C : ['C'], SR : ['SR'], UR : ['UR'] },
      packDesc  : ['C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'SR', 'SR', 'SR', 'SR', 'SR', 'SR', 'UR'],
    },

    'numh': PACK_HA,

    //TODO conver these
    'ha01': PACK_HA,
    'ha02': PACK_HA,
    'ha03': PACK_HA,
    'ha04': PACK_HA,
    'ha05': PACK_HA,
    'ha06': PACK_HA,
    'ha07': PACK_HA,
    'DRLG': PACK_HA,
    'DRL2': PACK_HA,
    'THSF': PACK_HA,
    'HSRD': PACK_HOLO_RARE,
    'WIRA': PACK_HOLO_RARE,
    'RYMP': {
      cardGroups: ['C', 'R', 'SR', 'UR', 'SCR'],
      groups  : { C : ['C'], SR : ['SR'], UR : ['UR'], 'SCR': ['SCR'] },
      packDesc  : ['C', 'C', 'C', 'C', 'C', 'C', 'C', 'R', 'SR', 'UR', 'SCR'],
    },


    //TODO get these packs
    'DRLU': PACK_HA,


    'LCGX': PACK_LC,
    'LCYW': PACK_LC,
    'LCJW': PACK_LC,
    'LC5D': PACK_LC,
    'PP01': PACK_PREMIUM, //3 SR, 2 SCR
    'PP02': PACK_PREMIUM,
    'SP13': PACK_STAR, // 3 C
    'MP14': PACK_MP,
    'MP15': PACK_MP,




    'DP1':  PACK_DP_6, //jaden yuki
    'DP2':  PACK_DP_6, //chazz
    'DP03': PACK_DP_6, //jaden yuki 2
    'DP04': PACK_DP_6, //zane truesdale
    'DP05': PACK_DP_6, //aster phoenix
    'DP06': PACK_DP_6, //jaden yuki 3
    'DP07': PACK_DP_6, //jesse anderson
    'DP08': PACK_DP_5, //yusei
    'DPYG': PACK_DP_5, //yugi
    'DP09': PACK_DP_5, //yusei 2
    'DPKB': PACK_DP_5, //kaiba
    'DP10': PACK_DP_5, //yusei 3
    'DP11': PACK_DP_5, //crow
    'DPBC': PACK_DP_5, //battle city
    'GLD1': PACK_GOLD,
    'GLD2': PACK_GOLD,
    'GLD3': PACK_GOLD,
    'GLD4': PACK_GOLD,
    'GLD5': PACK_GOLD,
    //there are regional variations with the Premium golds packs
    'PGLD-NA': {
      cardGroups: ['GR1', 'GR2', 'GSCR'],
      groups  : { GR1 : ['GR1'], GR2 : ['GR2'], 'GSCR': ['GSCR'] },
      packDesc  : [ 'GR1', 'GR2', 'GR2', 'GSCR', 'GSCR'],
    },
    //UK, AU, SP, PG
    'PGLD': {
      cardGroups: ['GR', 'GSCR'],
      groups  : { GR : ['GR'],'GSCR': ['GSCR'] },
      packDesc  : [ 'GR', 'GR', 'GR', 'GR', 'GR', 'GR', 'GR', 'GR', 'GR', 'GSCR', 'GSCR', 'GSCR', 'GSCR', 'GSCR', 'GSCR'],
    },
    //FR, GE, IT
    'PGLD-EU': {
      cardGroups: ['GR', 'GSCR'],
      groups  : { GR : ['GR'], 'GSCR': ['GSCR'] },
      packDesc  : [ 'GR', 'GR', 'GR', 'GR', 'GR', 'GR', 'SCR', 'SCR', 'SCR', 'SCR'],
    },

    'PGL2-NA': {
      cardGroups: ['GR1', 'GR2', 'GSCR'],
      groups  : { GR1 : ['GR1'], GR2 : ['GR2'], 'GSCR': ['GSCR'] },
      packDesc  : [ 'GR1', 'GR2', 'GR2', 'GSCR', 'GSCR'],
    },
    //UK, AU, SP, PG
    'PGL2': {
      cardGroups: ['GR1', 'GR2', 'GSCR'],
      groups  : { GR1 : ['GR1'], GR2 : ['GR2'], 'GSCR': ['GSCR'] },
      packDesc  : [ 'GR1', 'GR1', 'GR1', 'GR1', 'GR1', 'GR1', 'GR2', 'GR2', 'GR2',
              'GSCR', 'GSCR', 'GSCR', 'GSCR', 'GSCR', 'GSCR'],
    },
    //FR, GE, IT
    'PGL2-EU': {
      cardGroups: ['GR1', 'GR2', 'GSCR'],
      groups  : { GR1 : ['GR1'], GR2 : ['GR2'], 'GSCR': ['GSCR'] },
      packDesc  : [ 'GR1', 'GR1', 'GR1', 'GR1', 'GR2', 'GR2', 'GSCR', 'GSCR', 'GSCR', 'GSCR'],
    },

    //'PGL3'
    'PGL3': {
      cardGroups: ['GR', 'GSCR'],
      groups  : { GR : ['GR'], 'GSCR': ['GSCR'] },
      packDesc  : [ 'GR', 'GR', 'GR', 'GSCR', 'GSCR'],
    },


    //'DB1':
    //'DB2':
    //'DR1':
    //'DR2':
    //'DR3':
    //'DR04':
    //1-2 rares in a pack
    // 6M, 3T, 3S

    //'RP01':
    //'RP02' - 9 (8C + 1)

    //'DLG1' //11 C + 1
    //'MIL1' // 4C + 1 (R, SR, UR)
  },
};