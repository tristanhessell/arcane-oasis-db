'use strict';
/*eslint-env node, es6*/
require('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const app = express();

const winston = require('winston');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('combined'));
app.use('/', require('./routes/'));

app.listen(process.env.PORT, () => {
  winston.info(`Arcane Oasis Card DB Service: Port ${process.env.PORT}`);
});

//need to export so you can test it with Super Test
module.exports = app;