'use strict';
/*eslint-env node, es6, mocha*/

require('dotenv').config();
const request = require('supertest');
const server = require('../index.js');

const dbSetup = require('../util/sendCardsToMongo.js')('cardData', process.env.MONGODB_URI);

//TODO make the tests more descriptive in that they
// test structure of the returned content

//testing list routes
describe('List Routing', () => {
  before((done)=> {
    dbSetup.clean()
      .then(dbSetup.load)
      .then(done);
  });

  after((done)=> {
    dbSetup.clean().then(done);
  });

  it('GET should error if the game type is invalid ', (done) => {
    request(server)
      .get('/games/ASD/drafts/booster/lists/')
      .expect('Content-Type', /json/)
      .expect(404, {
        error: `invalid game type 'asd'`,
      }, done);
  });

  it('GET should error if the draft type is invalid ', (done) => {
    request(server)
      .get('/games/ygo/drafts/BNA13/lists/')
      .expect('Content-Type', /json/)
      .expect(404, {
        error: `invalid draft type 'bna13'`,
      }, done);
  });

  it('GET should get the list of lists', (done) => {
    request(server)
      .get('/games/ygo/drafts/booster/lists/')
      .expect('Content-Type', /json/)
      .expect(200, done);
  });

  it('GET should get the contents of a list', (done) => {
    request(server)
      .get(`/games/ygo/drafts/booster/lists/Legend of Blue Eyes`)
      .expect('Content-Type', /json/)
      .expect(200, done);
  });


  it('GET should error if the list name doesnt exist', (done) => {
    request(server)
      .get('/games/ygo/drafts/booster/lists/lobd')
      .expect('Content-Type', /json/)
      .expect(400, {
        error: `list 'lobd' not found`,
      }, done);
  });


  it('POST should create a list that doesnt exist', (done) => {
    const body = {
      name: 'lobBAH',
      list: [],
    };

    request(server)
      .post('/games/ygo/drafts/booster/lists/')
      .send(body)
      .expect(204, done);
  });

  xit('POST should error if you create a list that already exist', (done) => {
    const body = {
      name: 'Legend of Blue Eyes',
      list: [],
    };

    request(server)
      .post('/games/ygo/drafts/booster/lists/')
      .send(body)
      .expect('Content-Type', /json/)
      .expect(400, {
        error: `Cannot add list 'Legend of Blue Eyes' already exists`,
      }, done);
  });

  it('PUT should replace a list that exists', (done) => {
    request(server)
      .put('/games/ygo/drafts/booster/lists/Legend of Blue Eyes')
      //the body of the POST request
      .expect('Content-Type', /json/)
      .expect(200, {
        list: `list sucessfully updated`,
      }, done);
  });

  it('PUT should error if you try to update a non-existent list', (done) => {
    request(server)
      .put('/games/ygo/drafts/booster/lists/lobss')
      //the body of the POST request
      .expect('Content-Type', /json/)
      .expect(400, {
        error: `list 'lobss' not found`,
      }, done);
  });

  it('DELETE should delete a list that exists', (done) => {
    request(server)
      .delete('/games/ygo/drafts/booster/lists/Legend of Blue Eyes')
      .expect('Content-Type', /json/)
      .expect(200, {
        list: `List 'legend of blue eyes' sucessfully deleted`,
      }, done);
  });

  it('DELETE should error if you try to delete a list that doesnt exists', (done) => {
    request(server)
      .delete('/games/ygo/drafts/booster/lists/tshd13')
      .expect('Content-Type', /json/)
      .expect(400, {
        error: `list 'tshd13' not found`,
      }, done);
  });
});


