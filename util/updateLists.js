'use strict';
/*eslint-env node, es6*/

//quick function to change the structure of the base json files to ():
/*
    {
        name: 'Force of the Breaker',
        shortName: 'FOTB',
        list: [],
    }

 */
// all sets have a name and list, can have a shortName

const fs = require('fs');
const path = require('path');

const nameMap = {
  abpf        : 'Absolute Powerforce',
  abyr        : 'Abyss Rising',
  anpr        : 'Ancient Prophecy',
  ast        : 'Ancient Sanctuary',
  bp01        : 'Battle Pack: Epic Dawn',
  bp02        : 'Battle Pack 2: War of the Giants',
  bp03        : 'Battle Pack 3: Monster League',
  bpw2        : 'Battle Pack 2: Round 2',
  cblz        : 'Cosmo Blazer',
  cdip        : 'Cyberdark Impact',
  crms        : 'Crimson Crisis',
  cros        : 'Crossed Souls',
  crv        : 'Cybernetic Revolution',
  csoc        : 'Crossroads of Chaos',
  dcr        : 'Dark Crisis',
  drev        : 'Duelist Revolution',
  duea        : 'Duelist Alliance',
  een        : 'Elemental Energy',
  eoj        : 'Enemy of Justice',
  exvc        : 'Extreme Victory',
  fet        : 'Flames of Eternity',
  fotb        : 'Force of the Breaker',
  gaov        : 'Galactic Overlord',
  genf        : 'Generation Force',
  glas        : 'Gladiators Assault',
  ioc        : 'Invasion of Chaos',
  jotl        : 'Judgement of the Light',
  lob        : 'Legend of Blue Eyes White Dragon',
  lod        : 'Legacy of Darkness',
  lodt        : 'Light of Destruction',
  lon        : 'Labrynth of Nightmare',
  ltgy        : 'Lord of the Tachyon Galaxy',
  lval        : 'Legacy of the Valiant',
  mfc        : 'Magicians Force',
  mrd        : 'Metal Raiders',
  nech        : 'The New Challengers',
  numh        : 'Number Hunters',
  orcs        : 'Order of Chaos',
  pgd        : 'Pharonic Guardian',
  phsw        : 'Photon Shockwave',
  potd        : 'Power of the Duelist',
  prio        : 'Primal Origin',
  psv        : 'Pharohs Servant',
  ptdn        : 'Phantom Darkness',
  rds        : 'Rise of Destiny',
  redu        : 'Return of the Duelist',
  rgbt        : 'Raging Battle',
  sece        : 'Secrets of Eternity',
  shsp        : 'Shadow Specters',
  sod        : 'Soul of the Duelist',
  soi        : 'Shadow of Infinity',
  sovr        : 'Stardust Overdrive',
  srl        : 'Spell Ruler',
  stbl        : 'Startstrike Blast',
  ston        : 'Stike of Neos',
  stor        : 'Storm of Ragnarok',
  taev        : 'Tactical Evolution',
  tdgs        : 'The Duelist Genesis',
  tlm        : 'The Lost Millenium',
  tshd        : 'The Shining Darkness',
}






const loadListIntoDB = (listType) => {
    const pathToFolder = path.join('cardData', 'ygo', listType);

    fs.readdir(pathToFolder, (err, files) => {
        files.forEach((filename) => {
            const filePath = path.join(pathToFolder, filename);

            fs.readFile(filePath, 'utf8', (readErr, listData) => {
                if (readErr) throw readErr;

                const list = JSON.parse(listData).list;

                //add the list to the DB
                fs.writeFile(path.join(pathToFolder, filename), JSON.stringify({
                    name: nameMap[filename],
                    shortName: filename,
                    list,
                }));
            });
        });
    });
};

//loadListIntoDB('booster');


const load = (listType) => {
    const pathToFolder = path.join('cardData', 'ygo', listType);

    fs.readdir(pathToFolder, (err, files) => {
        files.forEach((filename) => {
            const filePath = path.join(pathToFolder, filename);

            fs.readFile(filePath, 'utf8', (readErr, listData) => {
                if (readErr) throw readErr;

                const list = JSON.parse(listData);

                //add the list to the DB
                fs.writeFile(path.join(pathToFolder, filename), JSON.stringify(list.list, null, 2));
            });
        });
    });
};

load('cube');
load('booster');
