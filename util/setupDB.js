'use strict';
/*eslint-env node, es6*/

//commandline variation of sendCardsToMongo
//sendCards folderPathToCardData mongoUrl clearDBFlag
//eg node util\setupDB.js clean

let dataFolderPath = './cardData';
let mongoUrl = process.env.MONGODB_URI;

const clearDB = (process.argv[2] === 'clean');
const dbWrapper = require('./sendCardsToMongo.js')(dataFolderPath, mongoUrl)

if (clearDB) {
  dbWrapper.clean()
    .then(dbWrapper.load);
  return;
}
dbWrapper.load();
