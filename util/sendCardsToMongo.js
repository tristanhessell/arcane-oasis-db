'use strict';
/*eslint-env node, es6*/

const MongoClient = require('mongodb').MongoClient;

const CARD_COLLECTION_NAME = 'CARD';
const LIST_COLLECTION_NAME = 'LIST';

const fs = require('fs');
const path = require('path');

//TODO eventaually, need to feed in cards via their own (massive list)
//Lists will only contain the card name and rarity - if booster list

module.exports = exports = (FOLDER_ROOT, MONGO_URL) => {
  const removeRarity = (card) => {
    delete card.rarity;
    return card;
  }

  const convertListCard = (inCard)  => {
    return {
      name: inCard.name,
    };
  };

  //for booster lists - each card should have a rarity
  const convertBoosterCard = (inCard) => {
    return {
      name: inCard.name,
      rarity: inCard.rarity,
    };
  };

  const sendCards = (db, cards) =>
    db.collection(`ygo-${CARD_COLLECTION_NAME}`)
      .insert(cards)
      .then((writeObj) => {
        if (writeObj.result.ok) {
          console.log('Write cards OK');
          return;
        }
        console.log('write not cards OK');
      });

  const sendList = (db, listObj) =>
    db.collection(`ygo-${LIST_COLLECTION_NAME}`)
      .insertOne(listObj)
      .then((writeObj) => {
        if (writeObj.result.ok) {
          console.log(`Write list: ${listObj.name} OK`);
          return;
        }
        console.log('write list not OK');
      });


  const readFile = (filePath, sendPromise) => new Promise((resolve, reject) => {
    fs.readFile(filePath, 'utf8', (readErr, inList) => {
      if (readErr) {
        reject(readErr);
        return;
      }
      sendPromise(JSON.parse(inList)).then(() => resolve());
    });
  });



  const sendCardsPromise = (db) => (cards) => sendCards(db, cards.list.map(removeRarity));

  //should be able to remove listType by this point
  const sendListPromise = (cardConverter, draftFormat) => (db) => (list) => sendList(db, {
    name: list.name,
    shortName: list.shortName,
    draftFormat: draftFormat,
    list: list.list.map((card) => cardConverter(card)),
  });

  const sendToMongo = (folderPath, sendPromise) => new Promise((resolve) => {
    MongoClient.connect(MONGO_URL, (err, db) => {
      fs.readdir(folderPath, (readErr, files) => {
        const promises = files.map((filename) => readFile(path.join(folderPath, filename), sendPromise(db)));

        Promise.all(promises).then(() => {
          db.close();
          resolve();
        });
      });
    });
  });

  const dropCollectionIfExists = (db, collectionName) => new Promise((resolve) => {

    db.collection(collectionName, {strict: true}, (err, collection) => {
      if (err) {
        console.log(`Cannot drop collection '${collectionName}': ${err}`);
        resolve();
        return;
      }

      console.log(`Dropped collection '${collectionName}'`);
      collection.drop()
      resolve();
    });
  });

  const clean = () => new Promise((resolve, reject) => {
    MongoClient.connect(MONGO_URL, (err, db) => {
      if (err) throw err;

      dropCollectionIfExists(db, `ygo-${LIST_COLLECTION_NAME}`)
        .then(() => dropCollectionIfExists(db, `ygo-${CARD_COLLECTION_NAME}`))
        .then(() => addConstraints(db))
        .then(() => {
          db.close();
          resolve();
        })
        .catch((errr) => {
          db.close();
          console.log(`clean error: ${errr}`);
          reject();
        })
    });
  });

  //TODO use promise.all?
  const addConstraints = (db) => {
    const listCollection = db.collection(`ygo-${LIST_COLLECTION_NAME}`);
    console.log('adding constraints');

    return listCollection.createIndex({name: "text"})
      .then(() =>
        db.collection(`ygo-${LIST_COLLECTION_NAME}`)
          .createIndex({name: 1, owner: 1}, {unique: true})
      )
      .then(() =>
        db.collection(`ygo-${CARD_COLLECTION_NAME}`)
          .createIndex({name: "text"})
      )
      .then(() => {
        console.log('contraints added');
      });
  };

  const load = () => Promise.resolve()
    .then(() => sendToMongo(path.join(FOLDER_ROOT, 'ygo', 'cube'), sendListPromise(convertListCard, 'cube')))
    .then(() => sendToMongo(path.join(FOLDER_ROOT, 'ygo', 'booster'), sendListPromise(convertBoosterCard, 'booster')))
    .then(() => sendToMongo(path.join(FOLDER_ROOT, 'ygo', 'booster'), sendCardsPromise))
    .catch(console.log);

  return {
    load,
    clean,
  };
};