'use strict';
/*eslint-env node, es6*/

const filePath = 'cardData/ygo/booster';
const fs = require('fs');

const boosterData = require('../server/YGOBoosterData.js').packSetting;

const VALID_CARD_TYPES = ['Monster', 'Spell', 'Trap'];
const VALID_MONSTER_TYPES = ['Effect', 'Ritual', 'Fusion', 'Normal', 'Synchro', 'Xyz', 'Pendulum'];

//read the files and make sure that all the cards are valid
//All: name, cardType, 
//rarity should be 

const isCardValid = (setName, card) => {
	//if the card has the correct type
    if (card.name && VALID_CARD_TYPES.indexOf(card.cardType) !== -1) {
        if ('Monster' === card.cardType) {
            //check that the monster type is valid AND
            //that the card rarity falls into the groups specified in the booster data
            return card.monsterType.every((monType) => VALID_MONSTER_TYPES.indexOf(monType) !== -1) && 
                boosterData[setName].cardGroups.indexOf(card.rarity) !== -1;
        }
        return true;
    }

    return false;
};

fs.readdir(filePath, (err, fileNames) => {
    fileNames.forEach((setName) => {
        fs.readFile(`${filePath}/${setName}`,(readErr, cardsStr) => {
            const cards = JSON.parse(cardsStr);
            cards.forEach((card) => {
                if (!isCardValid(setName, card)) {
                    console.log(`Err: Set: '${setName}' with card '${card.name}'`);
                }
            });

        });
    });
});