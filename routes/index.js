'use strict';
/*eslint-env node, es6*/

const express = require('express');
const router = express.Router();

//TODO move these hard coded parts into something like a config file
const GAME_TYPES = ['ygo'];
const DRAFT_TYPES = ['cube', 'booster'];

//validate the params (if they are in the request)
router.param('gameType', (req, res, next, inType) => {
  const gameType = inType.toLowerCase();

  if (!GAME_TYPES.includes(gameType)) {
    res.status(404).send({
      error: `invalid game type '${gameType}'`,
    });
    return;
  }

  req.gameType = gameType;
  next();
});

//validate the params (if they are in the request)
router.param('draftType', (req, res, next, inType) => {
  const draftType = inType.toLowerCase();

  if (!DRAFT_TYPES.includes(draftType)) {
    res.status(404).send({
      error: `invalid draft type '${draftType}'`,
    });
    return;
  }

  req.draftType = draftType;
  next();
});

//add the endpoints to the router
//the child routers MUST use express.Router({mergeParams: true})
////if they are to use the params from their parent

//dont actually use card routes here
// router.use('/games/:gameType/cards', require('./cardRoutes.js'));
router.use('/games/:gameType/drafts/:draftType/lists', require('./listRoutes.js'));

module.exports = router;
