'use strict';
/*eslint-env node, es6*/

const express = require('express');
const router = express.Router({mergeParams: true});
const dbClient = require('../dbClient.js');
const winston = require('winston');

//validate the params (if they are in the request)
router.param('name', (req, res, next, inName) => {
  req.name = inName.toLowerCase();
  next();
});

//get the lists for a given gametype & draftype
router.get('/', (req, res) => {
  const {gameType, draftType} = req;
  const {owner} = req.query; //Temporary

  dbClient.getLists(gameType, draftType, owner)
    .then((lists) => {
      res.send(lists);
    })
    .catch((error) => {
      winston.error(error)
      res.status(400).send({
        error,
      });
    });
});

//get the contents of a list
//send 404 if not found
router.get('/:name', (req, res) => {
  const {gameType, draftType, name} = req;
  const {owner} = req.query; //Temporary

  dbClient.getList(gameType, draftType, name, owner)
    .then((list) => {
      res.send({
        owner,
        list,
      });
    })
    .catch((error) => {
      winston.error(error)
      res.status(400).send({
        error,
      });
    });
});

//update list
//should send 404 if not found
//TODO change this to returning status
router.put('/:name', (req, res) => {
  const {gameType, draftType, name} = req;
  const {list, owner} = req.body;

  dbClient.updateList(gameType, draftType, name, list, owner)
    .then(() => {
      res.send({
        'list': 'list sucessfully updated',
      });
    })
    .catch((error) => {
      winston.error(error)
      res.status(400).send({
        error,
      });
    });
});

//change this with the name in the body
//add list (or error if the list already exists)
////TODO should send 409 if list already exists
router.post('/', (req, res) => {
  const {gameType, draftType} = req;
  const {name, list, owner} = req.body;

  dbClient.addList(gameType, draftType, name, list, owner)
    .then(() => {
      res.sendStatus(204);
    })
    .catch((error) => {
      winston.error(error)
      res.status(400).send({
        error,
      });
    });
});

//TODO change the sendStatus
router.delete('/:name', (req, res) => {
  const {gameType, draftType, name} = req;
  const {owner} = req.body;

  dbClient.deleteList(gameType, draftType, name, owner)
    .then(() => {
      res.send({
        list: `List '${name}' sucessfully deleted`,
      });
    })
    .catch((error) => {
      winston.error(error)
      res.status(400).send({
        error,
      });
    });
});

module.exports = router;
