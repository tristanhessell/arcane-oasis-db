'use strict';
/*eslint-env node, es6*/

const express = require('express');
const router = express.Router({mergeParams: true});
const dbClient = require('../dbClient.js');
const winston = require('winston');

//add card(s) to database
//should send 201 if created correctly
//TODO technically im updating a collection by adding the cards - will need to think this through better
router.post('/', (req, res) => {
  const gameType = req.gameType;
  const cards  = req.body.cards;

  dbClient.addCards(gameType, cards)
    .then(() => {
      res.send({
        cards: 'cards sucessfully added',
      });
    })
    .catch((error) => {
      winston.error(error);
      res.status(400).send({
        error,
      });
    });
});

module.exports = router;