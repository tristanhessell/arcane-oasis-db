'use strict';
/*eslint-env node, es6*/

const MongoClient = require('mongodb').MongoClient;
const url = process.env.MONGODB_URI;
const CARD_COLLECTION_NAME = 'CARD';
const LIST_COLLECTION_NAME = 'LIST';

const winston = require('winston');

//TODO decide what to do with errors here
// send an error object will all the errors?

const addCards = (gameType, cards) =>
  new Promise((resolve, reject) => {
    MongoClient.connect(url, (err, db) => {
      if (err) {
        reject({
          error: err,
        });

        return;
      }
      //TODO why did i decide to do it like this? find out if a single .insert() will work
      const cardPromises = cards.map((card) => addOneCard(db, gameType, card));

      Promise.all(cardPromises)
        .then((writeObjs) => {
          //get the write objects that are errors
          const failedWrites = writeObjs
            .filter((writeObj) => !writeObj.result.okay)
            .map((writeObj) => writeObj.writeError);

          if (failedWrites.length !== cardPromises.length) {
            reject({
              error: `Some cards failed to save for game '${gameType}'`,
              failedWrites,
            });
            return;
          }
          resolve({
            'message': `${cardPromises.length} cards saved for game '${gameType}'`,
          });
          db.close();
        });
    });
  });

//given a list of card names, return a list of card objects
//if the card doesnt exist, it is returned as a default card
const getCards = (gameType, cardNames, properties) =>
  new Promise((resolve, reject) => {
    MongoClient.connect(url, (err, db) => {
      if (err) {
        reject({
          error: err,
        });

        return;
      }

      getCardsFromList(db, gameType, cardNames, properties)
        .then((cards) => {
          db.close();
          if (cards === null) {
            reject('cards not found');
          } else {

            resolve(cards);
          }
        });
    });
  });


//given the correct params, return a list of listnames
const getLists = (gameType, draftFormat) =>
  new Promise((resolve, reject) => {
    MongoClient.connect(url, (err, db) => {
      if (err) {
        reject({
          error: err,
        });

        return;
      }

      findLists(db, gameType, {draftFormat})
        .toArray((findErr, lists) => {
          db.close();
          if (findErr) {
            reject({
              error: findErr,
            });

            return;
          }
          // resolve(lists.map(list => list.name).sort());
          resolve(lists.map(list => list).sort());
        });
    });
  });

//get the list and then get the cards in that list
//You only ever need to a list of cards when you need the cards gameType too
const getList = (gameType, draftFormat, name, owner) =>
  new Promise((resolve, reject) => {
    MongoClient.connect(url, (err, db) => {
      if (err) {
        reject({
          error: err,
        });

        return;
      }

      findList(db, gameType, {draftFormat, owner, $text: {$search: `"${name}"`}})
        .then((listObj) => {
          if (listObj === null) {
            reject(`list '${name}' not found`);
            return;
          }

          getCardsFromList(db, gameType, listObj.list)
            .then((cards) => {
              db.close();
              if (cards === null) {
                reject(' not found'); //TODO update this message
                return;
              }
              resolve(cards);
            }).catch((err) => {
              winston.error(err);
            });
        });
    });
  });

const getCardsFromList = (db, gameType, list, properties) =>
  new Promise((resolve) => {
    const cardProjection = convertArrayToProjectionObject(properties);
    const cardPromises = list.map((card) => findCard(db, gameType, {name: card.name}, cardProjection));

    Promise.all(cardPromises).then((cards) => {
      const strippedCards = cards.map((card, index) => {
        //if you can't find the card, just return an empty card object with just a name
        if (card === null) {
          winston.error(`couldnt find card ${list[index].name}`);
          return defaultCard(list[index].name);
        }

        //add the rarity to the card if the list has rarities
        card.rarity = list[index].rarity;

        return card;
      });

      resolve(strippedCards);
    });
  });



const addList = (gameType, draftFormat, name, list, owner) =>
  new Promise((resolve, reject) => {
    MongoClient.connect(url, (err, db) => {
      if (err) {
        reject({error: err});
        return;
      }

      addOneList(db, gameType, {name, owner}, {draftFormat, name, list, owner})
        .then((writeObj) => {
          db.close();
          if (writeObj.result.ok) {
            resolve(`List '${name}' added`);
            return;
          }
          reject(`Cannot add list: '${name}' db error`);
        });
    });
  });


const updateList = (gameType, draftFormat, name, list, owner) =>
  new Promise((resolve, reject) => {
    MongoClient.connect(url, (err, db) => {
      if (err) {
        reject({
          error: err,
        });

        return;
      }

      replaceList(db, gameType, {draftFormat, owner, $text: {$search: `"${name}"`}}, {gameType, draftFormat, name, list})
        .then((writeObj) => {
          db.close();

          if (writeObj.modifiedCount === 1) {
            resolve(`list ${name} updated`);
            return;
          }

          reject(`list '${name}' not found`);
        });
    });
  });

const deleteList = (gameType, draftFormat, name) =>
  new Promise((resolve, reject) => {
    MongoClient.connect(url, (error, db) => {
      if (error) {
        reject({
          error,
        });
        return;
      }

      removeList(db, gameType, {draftFormat, $text: {$search: `"${name}"`}})
        .then((writeObj) => {
          db.close();
          if (writeObj.result.n !== 0) {
            resolve(`List ${name} deleted`);
            return;
          }
          reject(`list '${name}' not found`);
        });
    });
  });

//true param removes one list
const removeList = (db, gameType, filters) => db.collection(`${gameType}-${LIST_COLLECTION_NAME}`)
  .remove(filters, true);

const replaceList = (db, gameType, filters, replacement) => db.collection(`${gameType}-${LIST_COLLECTION_NAME}`)
  .replaceOne(filters, replacement);

const addOneList = (db, gameType, identifier, listObj) => db.collection(`${gameType}-${LIST_COLLECTION_NAME}`)
  .updateOne(identifier, listObj, {upsert: true});

const findCard = (db, gameType, searchFilter, projection) => db.collection(`${gameType}-${CARD_COLLECTION_NAME}`)
  .findOne(searchFilter, projection);

const findList = (db, gameType, filters) => db.collection(`${gameType}-${LIST_COLLECTION_NAME}`)
  .findOne(filters, {_id: 0});

const findLists = (db, gameType, filters) => db.collection(`${gameType}-${LIST_COLLECTION_NAME}`)
  .find(filters,{_id: 0, name: 1, owner: 1});
//TODO what does the owner: filters.owner part for?


const addOneCard = (db, gameType, card) => db.collection(`${gameType}-${CARD_COLLECTION_NAME}`)
  .insertOne(card);

module.exports = {
  getCards,
  getLists,
  getList,
  addList,
  addCards,
  updateList,
  deleteList,
};



//BUSINESSY STUFF

//take in an array and turn it into a MongoDB
//ALWAYS show the name - even if they dont specify the card
//projection object
const convertArrayToProjectionObject = (arr) => {
  const obj = { _id: 0 };

  //if there are any properties, there needs to be the name
  if (arr) {
    obj.name = 1;
    arr && checkCardProperties(arr).forEach((property) => {
      obj[property] = 1;
    });
  }

  return obj;
}


//check that the provided array are in the set of properites that the gametype requires
//for now, only 'name' & 'cardType' can be used - TODO implement a proper system to determine this
const checkCardProperties = (propertyArray) => propertyArray.filter((prop) => prop === 'name' || prop === 'cardtype');

const defaultCard = (name) => ({ name });
