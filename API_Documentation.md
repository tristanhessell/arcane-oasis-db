#API Documentation#

All of the endpoints described in this document are prepended with the following url:
```
/games/:gameType/drafts/:draftType/lists
```

An example (full) URL call for updating a list:
```
POST {hosturl}/games/ygo/drafts/cube/lists/strike of neos/
```

##Endpoints##

GET /

Query:

owner: Username of the creator of the lists
(Optional)
Gets all the lists for the specified game & draft type, that are owned by the specified owner AND any lists that are public lists


200: []
400: {error}

###Example:###

*Request*
```
/?owner=juddy
```

*Response*
```
status code: 200
{

}
```

GET /:name

Query Parameters:
owner: Username of the creator of the lists (Optional)

Gets the specified list for the specified game and draft type, created by the specified owner
If the owner is not specified, the list is taken from the public lists.

200: {owner, list}
400: {error}

###Example:###

*Request*
```
/the shining darkness/?owner=ledankmemer
```
*Response*
```
status code: 200
{

}
```

PUT /:name

Body Parameters:

owner Username of the creator of the lists (Mandatory)
list (Mandatory)


Updates the specified list from the specified game and draft type. Fails if there is no list of the specified name with the specified owner.
200: {}
400: {error}

###Example:###

*Request*
```
/list name/
{
  owner: '',
  list: [],
}
```
*Response*
```
status code: 200
{

}
```

POST /
Body Parameters:
name (Mandatory)
list (Mandatory)
owner  Username of the creator of the lists (Mandatory)

Adds a list of the specified name.
If the list already exists, the list is replaced.


204:
400: {error}

###Example:###

*Request*
```
/
{
  name: '',
  list: [],
  owner: '',
}
```
*Response*
```
status code: 204
{

}
```

DELETE /:name
Body:
    owner  Username of the creator of the lists (Mandatory)

Removes a list of the specified name that was created by the specified owner.
Fails if the list doesnt exist or doesnt have the specified owner.
Not including owner in the request will fail, as all public lists cannot be removed.

200: {}
400: {error}

###Example:###

*Request*
```
/i hate this list/
{
  owner: 'tdawg',
}
```
*Response*
```
status code: 200
{

}
```